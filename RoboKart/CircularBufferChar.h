#pragma once
#include "misc.h"
#include <string.h>
//#include <stm32f4xx_gpio.h>		// Include GPIO header
//#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
//#include <stm32f4xx_tim.h>		// Include Timer header
#include <stm32f4xx_usart.h>	// Include USART header
#define BUFFER_SIZE 256

// Circular buffer using fixed size char array
// - possible alternative using pointers and void (seems too costly for CPU)
// - http://stackoverflow.com/questions/827691/how-do-you-implement-a-circular-buffer-in-c
class CircularBufferChar
{
public:
	CircularBufferChar(bool canOverwrite = false);
	void Initialise(uint32_t uiPeripheral);	// Placeholder until I can find a better way: allow function to disable TX interrupts
	void Empty();						// Wipe all data (just reset indexes)
	bool PushBack(const char * item, uint16_t length);	// Copies to item to buffer and manages size
	void GetOldest(char * item);		// Copies single item from buffer to item and manages size
	void GetNewest(char * item);		// Copies single item from buffer to item (does not touch size)
	//bool GetNewestMul(char * item, uint16_t length); // Copies multiple items from buffer to item and wipes data (does not touch size)
	uint16_t GetCount();				// Returns num items in the buffer (.count)
	bool IsEmpty();						// Returns true if count = 0
	bool IsFull();						// Returns true if count = BUFFER_SIZE
protected:
	uint8_t buffer[BUFFER_SIZE];	// Buffer for char data
	uint16_t head;					// Location of newest item
	uint16_t tail;					// Location of oldest item
	uint16_t count;					// numItems in the buffer
	uint32_t uiPeripheral;			// Var to store the base address of the peripheral
	bool canOverwrite;				// Bool to allow new data to overwrite old (important to prioritize new data)
};