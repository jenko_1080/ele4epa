// Information from DM00031020.pdf (Reference Manual)
/*
Acronyms
RCC - Reset Clock Control
NVIC - Nested Vectored Interrupt Controller

Clocks
Primary
HSI - High Speed Internal
HSE - High Speed External
Main PLL Clock
Secondary
LSI RC - Low Speed Internal RC (Watchdog Timer)

Busses
AHB - Advanced High-performance Bus
APB - Advanced Peripheral Bus
Generated from the AHB via assosciated prescalers

Timers
P216
� If the APB prescaler is 1, the timer clock frequencies are set to the same frequency as that of the APB domain to which the timers are connected.
� Otherwise, they are set to twice (x2) the frequency of the APB domain to which the timers are connected.


*/
// Defines
//#define DEBUG

// Includes - STM32
#include <stm32f4xx_gpio.h>		// Include GPIO header
#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
#include <stm32f4xx_tim.h>		// Include Timer header
#include <stm32f4xx_spi.h>		// Include SPI header
#include <misc.h>				// Function prototypes for misc firmware library functions - (Nested Vectored Interrupt Controller) Functions and SysTick
								//  - Add-on to CMSIS functions ( Cortex Microcontroller Software Interface Standard)
#include <string.h>
#include <stdio.h>	/////// SWO Debug Messages
// Includes - RoboKart
#include "Settings.h"			// Core RoboKart settings struct
#include "Utilities.h"			// Useful functions
#include "CircularBufferChar.h"	// Circular buffer class
#include "Bluetooth.h"			// Bluetooth functions (USART / Interrupt)
#include "Drive.h"				// Drive and Steering motor control
#include "IMU.h"				// IMU interface
#include "GPS.h"				// GPS Interface

/////////////////
// Global Vars //
/////////////////
#include "GlobalVars.h"
// Settings vars
Settings settings;
// Bluetooth Vars
CircularBufferChar BT_TXBuffer = CircularBufferChar();
char cNewLine[3] = "\r\n";
char cCommandAccepted[20] = "OK!\r\n";
uint8_t cCommand[4] = "";
// Drive Vars
Drive drive;
IMU imu;
GPS gps;
BT bt;

// Function Prototypes
//bool initClockOutputPins();								// Initialise AF pins and function for HSE and SysClock
void InitMainTimer(uint16_t dTime);
void InitIO();

int main()
{
	// Turn on delay
	_delay_ms(1000);
	
	///////////////////////////////
	// Set Initial Core Settings //
	///////////////////////////////
	settings.isGeoFenced = false;
	settings.isManual = false;
	settings.driveEnabled = false;
	settings.reportGPS = true;
	settings.newGPS = false;
	strcpy(settings.GPSString, "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"); // Always boot in GGA
	settings.reportIMU = false;
	settings.setUpdateMillis(50);
	settings.isGPSValid = false;
	settings.GPSDistanceFromInitial = 0.0;
	settings.GPSDistanceFromInitialMax = 150.0;
	settings.GPSAge = 0;
	settings.GPSAgeMax = 2500; // 2.5 seconds is a long time for gps
	settings.CommandAge = 0;
	settings.CommandAgeMax = 1000; // 0.2 seconds and its goooooone

	///////////////////////////////////////
	// Initialise Components and Classes //
	///////////////////////////////////////
	bt.initialise();			// Initialise BT (Control / Diagnostics)
	imu.initialise();			// Initialise Gyro / Acceleromoter
	
	gps.initialise();			// Initialise GPS
	gps.registerBT(&bt);			// provide GPS class with BT address
	gps.GPS_Send(settings.GPSString, strlen(settings.GPSString));

	drive.Initialise(200, 200);	// Initialise Motors / Control

	// TESTING
	//Disable Brakes / cruise
	drive.setControlVars(false, false);

	// Init the one clock to rule them all
	InitMainTimer(1); // Interrupt every 1ms

	///////////////////
	// Main Function //
	///////////////////
	float data[3];

#ifdef DEBUG
	printf("Main -> Project Init Complete\r\n");
#endif

	// Post Init Delay
	_delay_ms(1000);

	// How many satellites?
//	char GPS_VTG[100] = "$PMTK314,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n";
//	char GPS_GSV[100] = "$PMTK314,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"; // satellites in view
//	char GPS_GGA[100] = "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"; // GGA
//	char GPS_GGAGSV[100] = "$PMTK314,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"; // GGA + SV
//	char GPS_FACTORY[100] = "$PMTK314,-1*04";
//	gps.GPS_Send(GPS_GGA, strlen(GPS_GGA));

	for (;;)
	{	
//		GPIO_ToggleBits(GPIOD, GPIO_Pin_12);
//		imu.getAccelData(data);
//		_delay_ms(1000);
//		imu.getGyroData(data);
//		_delay_ms(1000);
//		imu.getMagData(data); // Working? - check conversion
//		_delay_ms(1000);
		gps.ParseString();
		bt.ParseString(drive, settings);
	}
}



////////////////////////////////
// Interrupt Service Routines //
////////////////////////////////
// Main Drive Control through interrupt / Timer 4
extern "C" void TIM4_IRQHandler()							// Use the ISR for Timer 2 (specified in startup_stm32f4xx.c) - extern "C" is used due to C++ application
{
	static uint8_t driveDecay;	// Set a variable to decay each command
	static uint8_t count = 0;	// Counter variable
	static uint8_t updateMillis;
	char reportString[196] = "";
	uint8_t reportLength = 0;

	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)		// Check that TIM_IT_Update is valid (is this needed?)
	{
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);			// Clear interrupt bit (software doesn't automatically do it)
		
		// Increment required counters
		if (settings.GPSAge < settings.GPSAgeMax)
			++settings.GPSAge;
		if (settings.CommandAge < settings.CommandAgeMax)
			++settings.CommandAge;

		if (count == updateMillis)
		{
			// Run update functions
			drive.update();

			// Reset counter
			count = 0;

			// only refresh once every count cycle, saves instructions
			updateMillis = settings.getUpdateMillis();
		}
		else
		{
			if (count == updateMillis - 2)
			{
				// Trigger ADC conversion (the rest is handled by interrupt)
				ADC_SoftwareStartConv(ADC1);
			}

			++count;
		}
	}
}

// ISR For USART1 Interrupt (GPS)
extern "C" void USART1_IRQHandler()
{
	static char cTX = '\0';
	static char cRX = '\0';
	static char ucIterator = 0;

	// TX Interrupt
	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{
		// Send TX Data (Buffer manages interrupt)
		gps.TXBuffer.GetOldest(&cTX);
		USART_SendData(USART1, cTX);
		// Interrupt is automatically cleared upon write
	}

	// RX Interrupt
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		// Get RX Data
		cRX = USART_ReceiveData(USART1);
		gps.GPS_Recieve(cRX);
		// Interrupt is automatically cleared upon read
	}
}

////////////////////////////////
// Interrupt Service Routines //
////////////////////////////////
// ISR For USART2 (BT)
extern "C" void USART2_IRQHandler()
{
	static char cTX = '\0';
	static char cRX = '\0';
	//static char cRXCommand[2] = "\0"; // Max command length = 1
	//static char ucIterator = 0;

	// TX Interrupt
	if (USART_GetITStatus(USART2, USART_IT_TXE) != RESET)
	{
		// Send TX Data (Buffer manages interrupt)
		bt.TXBuffer.GetOldest(&cTX);
		USART_SendData(USART2, cTX);
		// Interrupt is automatically cleared upon write
	}
	// RX Interrupt
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		// Get RX Data
		cRX = USART_ReceiveData(USART2);
		bt.BT_Recieve(cRX);
		// Interrupt is automatically cleared upon read
	}
}

extern "C" void ADC_IRQHandler()
{
	// ADC1 Interrupt
	if (ADC_GetITStatus(ADC1, ADC_IT_EOC) != RESET)
	{
		// Get ADC Data
		uint16_t ADCIn = ADC_GetConversionValue(ADC1);
		
		// Put ADC Data in Drive class
		drive.setAccelValue(ADCIn);

		if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0)
		{
			drive.setReverse(true);
		}
		else
		{
			drive.setReverse(false);
		}

		// Interrupt is automatically cleared upon write
	}
}

///////////////
// Functions //
///////////////
void InitMainTimer(uint16_t dTime)
{
	// Set up for 50Hz interrupt (Motor Control)
	// - http://dohzer.blogspot.com.au/2013/04/stm32-system-and-timer-clock_7.html
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);	// Enable Timer 2 block (Give it a clock)
	TIM_TimeBaseInitTypeDef timerInitStruct;
	timerInitStruct.TIM_Prescaler = 84;						// Set timer prescaler to 40000 (sysclock divider)
	//  - Timer Tick Frequency => 84MHz / (83 + 1) = 1MHz
	timerInitStruct.TIM_Period = 1060 * dTime;				// Set timer period (reload value)
	//  - 1/2 Period => 84MHz / (4199 + 1) = 10kHz
	timerInitStruct.TIM_CounterMode = TIM_CounterMode_Up;	// Set timer counter mode to count up
	timerInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;		// Set clock division
	timerInitStruct.TIM_RepetitionCounter = 0;				// Set number of timer repetitions to 0 before update event is generated and number resets (only for TIM1 and TIM8)
	TIM_TimeBaseInit(TIM4, &timerInitStruct);				// Load init data into Timer 2
	TIM_Cmd(TIM4, ENABLE);										// Enable Timer2

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);				// Clear Timer 2 interrupt bit
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);				// Enable Timer 2 Interrupt

	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = TIM4_IRQn;				// Set the IRQ channel to TIM2
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;	// Set the interrupt priority, lower is more important
	nvicStructure.NVIC_IRQChannelSubPriority = 1;			// Set the interrupt sub-priority, lower is more important
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;				// Set the interrupt to enabled
	NVIC_Init(&nvicStructure);								// Send the struct to init the interrupt
}

// LED for proof!
void InitIO()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);	// Enable clock on port D

	// Init GPIO for Brake / Cruise / Reverse / Spare (PE8 - PE15)
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;			// Set pin(s) mode to Output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set pin(s) output type to push/pull
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		// Set pin(s) speed to reasonable level
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s) (Externally fitted)

	// Drive Motor Enable
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;				// Pick which pin(s) to init - Pin 7
	GPIO_Init(GPIOD, &GPIO_InitStruct);					// Set data on selected port (E) using struct
	GPIO_ResetBits(GPIOD, GPIO_Pin_12);
}