#pragma once

#include <misc.h>				// Function prototypes for misc firmware library functions - (Nested Vectored Interrupt Controller) Functions and SysTick
								//  - Add-on to CMSIS functions ( Cortex Microcontroller Software Interface Standard)
#include <stm32f4xx_gpio.h>		// Include GPIO header
#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
#include <stm32f4xx_spi.h>		// Include SPI header
#include <string.h>
#include <stdio.h>	/////// SWO Debug Messages

// IMU DEFINES //
/////////////////
// STM32 Pins
#define IMU_CS_XM GPIO_Pin_0
#define IMU_CS_G GPIO_Pin_1
#define IMU_CS_PINS IMU_CS_XM | IMU_CS_G
// Control Registers
#define CTRL_REG0_XM 0x1F
#define CTRL_REG1_XM 0x20
#define CTRL_REG2_XM 0x21
#define CTRL_REG3_XM 0x22
#define CTRL_REG4_XM 0x23
#define CTRL_REG5_XM 0x24
#define CTRL_REG6_XM 0x25
#define CTRL_REG7_XM 0x26
#define CTRL_REG1_G 0x20
#define CTRL_REG2_G 0x21
#define CTRL_REG3_G 0x22
#define CTRL_REG4_G 0x23
// Data Registers
#define OUT_X_L_A 0x28 // Accelerometer Low
#define OUT_X_L_G 0x28 // Gyroscope Low
#define OUT_X_L_M 0x08 // Magnetometer Low
#define OUT_TEMP_L_XM 0x05 // Temperature Low
// Scaling Factors
#define IMU_A_SCALE 0.00018310546875 // x/32768 (x = scale)
#define IMU_G_SCALE 0.007476806640625 //  x/32768 (x = scale)
#define IMU_M_SCALE 0.00006103515625 // x/32768 (x = scale - 2 in this config Gs)
#define IMU_T_SCALE 0.125 // 1/8 as per datasheet
// Debug
//#define DEBUG

/**
* \class IMU
*
* \brief IMU Data Interface Class
*
* This class is here to enable the easy access of consistent data from the Gyroscope and Accelerometers.
* Upon initialisation this class will configure the required pins and hardware to interface with the LSM9DS0 IMU.
*
* \author Michael Jenkins
* \date 28/09/2015
* \todo Update this class to use interrupts instead of polling
* \todo Add IMU interrupt for too much acceleration
* \todo Add IMU interrupt for flip (cut power)
* \todo Add functions to enable easy changing of scales (and recalc of scale factors)
*/

class IMU
{
public:
	/** \Brief IMU Constructor.
	*  Sets initial state (if required- currently not)
	*/
	IMU();

	/** \Brief IMU Constructor.
	* Sets up SPI hardware and other required components for object operation
	*/
	void initialise();

	/** \Brief Read Accelerometer Data
	* Read accel X, Y and Z data from IMU
	* \param[out] data returns 3 x 16 bit data array for X, Y and Z
	*/
	void getAccelData(float * fltData);
	
	/** \Brief Read Gyroscope Data
	* Read gyroscope X, Y and Z data from IMU
	* \param[out] data returns 3 x 16 bit data array for X, Y and Z
	*/
	void getGyroData(float * fltData);
	
	/** \Brief Read Magnetometer Data
	* Read magnetometer X, Y and Z data from IMU
	* \param[out] data returns 3 x 16 bit data array for X, Y and Z
	*/
	void getMagData(float * fltData);

	/** \Brief Read Magnetometer Data
	* Read magnetometer X, Y and Z data from IMU
	* \warning Temperature data from chip IS NOT calibrated, this must be done for an accurate reading
	* \param[out] data returns 3 x 16 bit data array for X, Y and Z
	*/
	void getTempData(float * fltData);
	
protected:
	// Initialisation Helper Functions //
	/////////////////////////////////////
	void initCSPins();	/*!< Initialise Chip Select Pins required for interface */
	void initSPI();		/*!< Initialise SPI Hardware */

	/** \Brief Initialise IMU with desired settings
	* Initialise IMU with desired settings and report on success.
	* Each setting written to the device will be read to ensure that the write was successful.
	* \return Boolean true on success, false on error
	*/
	bool initIMU();		

	// Data Access Functions //
	///////////////////////////
	
	/** \Brief Write a single byte in to IMU memory via SPI
	* Helper function to write a single byte in to IMU memory. This function configures necessary paramters and masking to assist with safe operation.
	* \note First byte structure: | Read | Increment | +Address+ |
	* \param[in] chip Defines which chip is to be written to (0 = XM, 1 = Gyro)
	* \param[in] address Location for data to be written
	* \param[in] data Data to be written in to chip
	*/
	void spiWriteByte(uint8_t chip, uint8_t address, uint8_t data);
	
	/** \Brief Read a single byte from IMU memory via SPI
	* Helper function to read a single byte from IMU memory. This function configures necessary paramters and masking to assist with safe operation.
	* Returns data via variable.
	* \note First byte structure: | Write | Increment | +Address+ |
	* \param[in] chip Defines which chip is to be written to (0 = XM, 1 = Gyro)
	* \param[in] address Location for data to be read
	* \return Data read from the chip
	*/
	uint8_t spiReadByte(uint8_t chip, uint8_t address);

	/** \Brief Read a single byte from IMU memory via SPI
	* Helper function to read a single byte from IMU memory. This function configures necessary paramters and masking to assist with safe operation.
	* Returns data via pointer.
	* \note First byte structure: | Write | Increment | +Address+ |
	* \param[in] chip Defines which chip is to be written to (0 = XM, 1 = Gyro)
	* \param[in] address Location for data to be read
	* \param[out] Data read from the chip
	*/
	void spiReadByte(uint8_t chip, uint8_t address, uint8_t * data);
	
	/** \Brief Read multiple bytes from IMU memory via SPI
	* Helper function to read multiple bytes from IMU memory. This function configures necessary paramters and masking to assist with safe operation.
	* Returns in to an array data via pointer, recurses up to length.
	* \note First byte structure: | Write | Increment | +Address+ |
	* \param[in] chip Defines which chip is to be written to (0 = XM, 1 = Gyro)
	* \param[in] address Location for data to be read
	* \param[out] Data read from the chip
	*/
	void spiReadBytes(uint8_t chip, uint8_t address, uint8_t * data, uint8_t length);

	/** \Brief Control chip select pins
	* Function to control chip select pins
	* \param[in] chip Defines which chip is to be enabled to (0 = XM, 1 = Gyro)
	*/
	void selectChip(uint8_t chip);

	/** \Brief Control SPI hardware (send / recieve data)
	* Function to control SPI hardware, send and recieve data.
	* \note To send clock without information, send data as 0x00.
	* \param[in] chip Defines which chip is to be enabled to (0 = XM, 1 = Gyro)
	*/
	uint8_t spiTransfer(uint8_t data);

	// Variables //
	///////////////
};