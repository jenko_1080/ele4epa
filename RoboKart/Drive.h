#pragma once

#include <misc.h>				// Function prototypes for misc firmware library functions - (Nested Vectored Interrupt Controller) Functions and SysTick
								//  - Add-on to CMSIS functions ( Cortex Microcontroller Software Interface Standard)
#include <algorithm>
#include <stm32f4xx_gpio.h>		// Include GPIO header
#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
#include <stm32f4xx_tim.h>		// Include Timer header
#include <stm32f4xx_dac.h>		// Include DAC header
#include <stm32f4xx_adc.h>		// Include ADC header
#include <misc.h>				// Function prototypes for misc firmware library functions - (Nested Vectored Interrupt Controller) Functions and SysTick
								//  - Add-on to CMSIS functions ( Cortex Microcontroller Software Interface Standard)
#include "Settings.h"			// Include settings struct
#include <stdio.h>
//#define DEBUG


/**
 * \class Drive
 * 
 * \brief Robot Drive Control class.
 * 
 * This class contains the control methods to ensure that motor torque is scaled to match steering angle.
 * There is also power systems control for these devices to start up and shut down components as required.
 *
 * \note Valid values for acceleration are 1.2 V to 3.2 V, these values are taken AFTER the op-amp
 *
 * \author Michael Jenkins
 * \date 11/10/2015
 * \todo Calibrate function for ADC accelerator voltage
 * \todo Implement command aging for remote commands
 */

class Drive
{
public:
	/** \Brief Drive Constructor.
	 *  Sets initial status for all variables, runs all initialisation functions to ensure ports are correctly set.
	 * \note Initial conditions for motor power is to be disabled.
	 * \param dTorque Maximum torque change per step
	 * \param dAngle Maximum angle change per step
	 */
	Drive();
	void Initialise(uint16_t dTorque, uint8_t dAngle);
	void InitEnablePins();			/*!< Initialise Enable output pin hardware */
	void InitPWM();					/*!< Initialise Steering Servo PWM hardware */
	void InitDAC();					/*!< Initialise Motor Torque DAC hardware */
	void InitADC();					/*!< Initialise Accelerator ADC hardware */
	void InitControlOutputPins();	/*!< Initialise Control output pin hardware */
	/** \Brief Turn off or on drive / steering motors.
	* Quick function to set steering and drive power
	* \param[in] driveEnable Boolean to indicate drive motor status
	* \param[in] steerEnable Boolean to indicate steer motor status
	*/
	void setPower(bool driveEnable, bool steerEnable);

	/** \Brief Setter method for desiredDriveTorque
	* \note will only set variable if drive motor is enabled
	* \param driveTorque final torque to be reached (negative indicates reverse)
	*/
	bool zeroControls();

	/** \Brief Turn off or on drive motors.
	* Turn off or on drive motors, all variables should be reset to zero position to ensure no sudden jolts.
	* \param driveEnable Boolean to indicate drive motor status
	*/
	void setDrivePower(bool driveEnable);

	/** \Brief Turn off or on steering motor.
	* Turn off or on steering motors, all variables should be reset to zero position to ensure no sudden jolts.
	* Timer 4 will not be disabled (steering increment)
	* \param steerEnable Boolean to indicate drive motor status
	*/
	void setSteerPower(bool steerEnable);

	/** \Brief Setter method for desiredSteerAngle
	* \note will only set variable if steering motor is enabled
	* \param steerAngle final angle to be reached
	*/
	bool setDesiredSteerAngle(uint8_t steerAngle);

	/** \Brief Setter method for desiredDriveTorque
	* \note will only set variable if drive motor is enabled
	* \param driveTorque final torque to be reached (negative indicates reverse)
	*/
	bool setDesiredDriveTorque(int16_t driveTorque);

	/** \Brief Update method to be run every time increment
	* Any changes to motion are to be controlled through this function, motors and steering will be incrementally updated based on changes in set in dTorque and dAngle
	* \note This controls core motion and steering increments
	*/
	void update();

	/** \Brief Setter function to set accelerator value as read by ADC
	* Accepts 12-bit unsigned value and stores it in Drive::accelValue
	* \param[in] newAccelValue new value to be stored
	*/
	void setAccelValue(uint16_t newAccelValue);

	/** \Brief Setter function to set reverse var from hardware
	* Setter function to set reverse var from hardware
	* \param[in] newReverse new value to be stored
	*/
	void setReverse(bool newReverse);

	/** \Brief Setter function to set control vars
	* Controls motor brake / cruise
	* \param[in] brake turn on braking
	* \param[in] cruise turn on cruise
	*/
	void setControlVars(bool brake, bool cruise);

	/** \Brief LocationArray for previously found location data.
	*/
	char locationArray[350][350];

protected:
	// Internal Functions //
	/** \Brief Trigger function to start ADC sampling
	* Trigger function that is intended to be called by the update loop, when called this will start the ADC process- the rest will be handled by the ADC ISR
	* \note this starts a process that is then completed by an interrupt
	*/
	void triggerADC();

	/** \Brief ISR function to collect ADC data
	* This function will be called by the ADC ISR, this will read and scale the ADC value in preparation for the next update loop
	* \note this function is called externally by the ADC ISR
	*/
	void readADC();

	/** \brief Helper function to set new current torque, angles and offset
	* This function will calculate the change in torque based on dTorque and dAngle
	* It is intented to be called by update()
	* \see { Drive::update }
	*/
	bool setCurrentAngleAndTorque();

	/** \brief Helper function to calculate drive wheels offset
	* \param angle angle to calculate drive differential for, expressed as a number between 101 and 249 (inclusive).
	* \return uint16_t of drive wheel differential offset
	*
	* This method will calculate and set the required drive wheel torque difference based on the steering angle.
	* Ackerman geometry will be taken in to account for this calculation.
	*/
	int16_t calculateDriveOffset(uint8_t angle);

	/** \brief Load Steering and torques in to related hardware registers
	* Load Steering Angle in to PWM Register and torque (+/-Offset) in to DAC registers
	*/
	bool loadTorqueAndAngle();

	bool driveEnable;	/*!< Indicates whether the drive motor is currently enabled or disabled */
	bool steerEnable;	/*!< Indicates whether the steering motor is currently enabled or disabled */

	// Time, torque and angle steps
	uint8_t dAngle;	/*!< Maximum change in steering angle per time step */
	uint16_t dTorque;	/*!< Maximum change in torque per time step */

	// Accelerator
	uint16_t accelValue;	/*!< current accelerator value as read by the ADC */

	// Current values
	uint8_t currentSteerAngle;	/*!< Holds the current steering angle (Valid from 101 to 249) */
	int16_t currentDriveTorque;	/*!< Holds the current drive torque, converted to analogue by DAC */

	uint8_t desiredSteerAngle;	/*!< Holds the desired steering angle (angle to be reached) */
	int16_t desiredDriveTorque;	/*!< Holds the desired drive torque (torque to be reached)*/

	int16_t driveOffset;	/*!< Holds the current drive torque offset, added to driveTorque to allow for steering differential */
	bool driveCruise;		/*!< Boolean to set the motor controllers to cruise */
	bool driveBrake;		/*!< Boolean to signal when the brakes are being used */
	bool driveReverse;		/*!< Boolean to signal when the reverse switch is on */

//	BT bt;
};