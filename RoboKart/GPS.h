#pragma once

// Includes
#include <misc.h>
#include <stm32f4xx_gpio.h>		// Include GPIO header
#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
#include <stm32f4xx_usart.h>	// Include USART header
#include <string.h>
#include <stdlib.h>				// atof()
#include <stdio.h>	/////// SWO Debug Messages
#include <float.h>
#include <CircularBufferChar.h>
#include <Bluetooth.h>	// for bt debug messages
#include <math.h>

#define DEGREE_TO_RAD 0.017453292519943295769236907684886
#define EARTH_RAD_M 6378160
/**
* \class GPS
*
* \brief Robot GPS location class.
*
* This class controls communication and location data relating to the GPS
* The class has the ability to send configuration messages and recieve and decode standard GPS strings
*
* \author Michael Jenkins
* \date 09/10/2015
* \todo Change function to extend a base USART class, merge with BT
*/
class GPS
{
public:
	/** \Brief GPS Constructor.
	*  Sets initial state (if required- currently not)
	*/
	GPS();

	/** \Brief GPS Initialisation function.
	* Sets up USART hardware and other required components for object operation.
	* \note Includes setup of circular buffer
	*/
	void initialise();

	/** \Brief GPS USART Recieve Function
	* Recieves bytes from USART and appends them to current recieve string. Upon receipt of \r\n (CRLF) this function will break the data in to individual components and clear recieve string.
	*/
	void GPS_Recieve(char byte);

	/** \Brief GPS USART Send Function
	* Validates data string and (if ok) adds to BT circular buffer which takes care of the rest
	*/
	void GPS_Send(char * string, uint16_t length);

	/** \Brief GPS String Parser
	* This function parses the recieved GPS string and stores the new data in the appropriate variables
	*/
	void ParseString();

	/** \Brief GPS TX Circular Buffer
	* Buffer that is used to send data or instructions to GPS device.
	* Stores TX data after it has been validated by the GPS_Send function.
	*/
	CircularBufferChar TXBuffer;

	/** \Brief Distance From Starting Point (On a sphere)
	* Calculates the distance from the starting lat / lon using haversine formula
	*/
	void calcDistanceFromStart();

	void registerBT(BT * newbt);
protected:
	void initUSARTPins();
	void initUSART(uint32_t uiBaudRate, uint16_t uiMode);

	// Variables //
	///////////////

	/** \Brief GPS Data Storage
	* Struct containing fields to store all GPS data
	* \todo ensure initialLat and initialLong are initialised to 0 on boot.
	*/
	struct GPSData {
		/** \Brief Initial Latitude - First Known Location
		* Variable to store first known location upon GPS fix. This variable can be used to calculate linear distance travelled from turn on and is included to enable geo-fencing shutdown in the case of a run-away kart.
		* \note Positive number indicates North, negative indicates south.
		*/
		double initialLat;

		/** \Brief Initial Longitude - First Known Location
		* Variable to store first known location upon GPS fix. This variable can be used to calculate linear distance travelled from turn on and is included to enable geo-fencing shutdown in the case of a run-away kart.
		* \note Positive number indicates East, negative indicates West.
		*/
		double initialLong;

		/** \Brief UTC Time
		* UTC time reported by GPS
		*/
		double UTCTime;

		/** \Brief Most Recent Latitude
		* Most recent latitude as indicated by GPS
		* \note Positive number indicates North, negative indicates south.
		*/
		double currentLat;

		/** \Brief Most Recent Longitude
		* Most recent longitude as indicated by GPS
		* \note Positive number indicates East, negative indicates West.
		*/
		double currentLong;

		/** \Brief Altitude above mean sea level
		* Altitude above mean sea level
		*/
		double mslAltitude;

		/** \Brief Reported speed over ground
		* Reported speed over ground (meters/sec - converted from knots)
		*/
		double groundSpeed;

		/** \Brief Reported speed over ground
		* Reported course over ground (degrees)
		*/
		double groundCourse;

		/** \Brief Position Fix
		* Reported Position Fix Indicator
		* - 0 = Fix not available or invalid
		* - 1 = GPS SPS Mode, fix valid
		* - 2 = Differential GPS, SPS Mode, fix valid
		* - 3-5 = Not Supported
		* - 6 = Dead Reckoning Mode, fix valid
		*/
		uint8_t positionFix;

		/** \Brief Indicates data in struct is valid
		* Flag to indicate whether conversion is complete and data in this struct is valid
		*/
		bool isComplete;

		/** \Brief First data indicator, for initial settings
		* First data indicator, for initial settings
		*/
		bool isFirst;

		/** \Brief Distance from inital point
		* distance from initial lat / long as calculated using haversine formula
		*/
		double distanceFromInitial;
	} GPSData;

	char GPSString[192];
	bool isGPSStringNew;

	char reportString[192];

	/** \Brief GPS RX Buffer
	* Struct containing a char array, counter and add function
	* Stores incoming RX data until CRLF is recieved and entire string can be interpreted.
	* \todo ensure currentChar is initialised to 0 on boot.
	*/
	struct RXBuffer {
		/** \Brief RXBuffer current location
		* integer pointing to next free slot, incremented after each char is added.
		*/
		uint8_t currentChar;

		/** \Brief CRC Value
		* 1 means first number in CRC, 2 is second. Handled by add().
		*/
		uint8_t currentCRC;
		
		/** \Brief RXBuffer data array
		* char array for all recieved data from GPS module.
		*/
		char data[192];

		/** \Brief Recieved CRC
		* Char to recieve CRC value after the end of a string
		*/
		char crcRx;

		/** \Brief CRC Storage
		* Variable initialised to 0 and XOR'd with each incoming byte
		*/
		char crcLocal;

		/** \Brief add char function
		* add function to ensure all new chars are correctly added and no overflow occurs.
		* \param[in] newChar new char to be added to data array
		* \return boolean to incidate success or failure. function will return false in the event the buffer is full.
		*/
		bool add(char newChar); // Add function, will control adding new chars

		/** \Brief String Verification Function
		* Check for \r\n as the most recently added two chars 
		* \return boolean to incidate success or failure. function will return false in the event the two newest chars aren't \r\n..
		*/
		bool checkComplete();

		/** \Brief "Move" string to new location in preparation for processing
		* Copy data to new char array in preparation for data processing, also reset pointer variable to allow new data to be recieved.
		*/
		void move(char * GPSString);
	} RXBuffer;

	BT * bt;
};