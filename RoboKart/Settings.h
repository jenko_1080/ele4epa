#pragma once 

/**
* \struct Settings
*
* \brief Core Settings Struct
*
* This struct contains the core settings for the system to be used through all classes that require such information.
* Data validation functions are implemented to ensure that safe ranges are obeyed in robot operation.
*
* \todo move core variables in to this function and implement control in other classes e.g. geoFenceDiameter
*
* \author Michael Jenkins
* \date 10/10/2015
*/
struct Settings {
public:
	bool isManual; /*!< Manual or Autonomous mode */
	bool isGeoFenced; /*!< Geofence enable - disable operation outside certain perimeter */
	bool driveEnabled; /*!< Enable power to motors */

	bool reportGPS; /*!< Report GPS through BT */
	bool GPSshowSats; /*!< Set GPS to show SV */
	bool newGPS; /*!< Var to let to GPS parser know that there is an update to send */
	char GPSString[100];

	bool reportIMU; /*!< Report IMU through BT */

	// Control vars for drive::update
	bool isGPSValid;
	uint32_t GPSAge; // Counter (used in main update loop) to count GPS age
	uint32_t GPSAgeMax; // maximum value for GPS age
	double GPSDistanceFromInitial;
	double GPSDistanceFromInitialMax;
	uint32_t CommandAge;	// couner used to age last command
	uint32_t CommandAgeMax;	// maximum value for command age

	uint8_t getUpdateMillis()
	{
		return updateMillis;
	}

	void setUpdateMillis(uint8_t newUpdateMillis)
	{
		if (newUpdateMillis > 100)
		{
			updateMillis = 100;
		}
		else if (newUpdateMillis < 5)
		{
			updateMillis = 5;
		}
		else
		{
			updateMillis = newUpdateMillis;
		}
	}
protected:
	uint8_t updateMillis; /*!< Count value to trigger update functions in core program */
};

extern Settings settings;