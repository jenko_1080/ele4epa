#include "Bluetooth.h"

BT::BT() : BTString(""), isBTStringNew(false)
{
}

void BT::initialise()
{
	// Set up RXBuffer
	RXBuffer.currentChar = 0;

	// Initialise USART Pins
	initUSARTPins();

	// Initialise USART
	initUSART(9600, USART_Mode_Rx | USART_Mode_Tx);

	// Configure Circular Buffer
	TXBuffer.Initialise(USART2_BASE);
}

void BT::BT_Recieve(char byte)
{
	// Loopback for user interactivity
	TXBuffer.PushBack(&byte, 1);

	// Add byte to buffer
	if (!RXBuffer.add(byte))
	{
		// Oh... the buffer is full... this is a problem
		// (or the first char is invalid)
		return;
	}

	// Check that string is complete
	if (RXBuffer.checkComplete())
	{
#ifdef DEBUG
		printf("BT::BT_Recieve -> String Recieved: %s\r\n", RXBuffer.data);
#endif

		// Validate String
		if (RXBuffer.crcRx != RXBuffer.crcLocal)
		{
			BT_Send(errorCRC, 30);
#ifdef DEBUG
	printf("BT::BT_Recieve CRC Error: 0x%2x and 0x%2x\r\n", RXBuffer.crcRx, RXBuffer.crcLocal);
#endif
		}
		else
		{
			// Set flag
			isBTStringNew = true;
		}

		// Copy Complete String
		RXBuffer.move(BTString);

		BT_Send("\r\n", 2);
	}
}

void BT::BT_Send(const char * string, uint16_t length)
{
	// Verify String (check for \r\n at end) - keep the terminal clean
	if (string[length - 2] != '\r' || string[length - 1] != '\n')
	{
		return;
	}

	// Add string to TX buffer
	TXBuffer.PushBack(string, length);
}

void BT::ParseString(Drive &drive, Settings &settings)
{
	// Check that the string is new
	if (!isBTStringNew)
	{
		return;
	}

	// Copy working string in to it's own buffer (prevent corruption)
	char lStr[192];
	char tStr[12];
	char retStr[50];
	retStr[0] = '\0';
	strcpy(lStr, BTString);

#ifdef DEBUG
	printf("BT::ParseString - Working on new string: %s\r\n", lStr);
#endif

	// String has been copied and is no longer 'new' to this function
	isBTStringNew = false;

	// Start at char 1, as 0 is '$'
	// Check for D, C or R
	// * If the string doesn't match one of these there is a problem that wasn't found in CRC... but is avoided here
	if (lStr[1] == 'R' && !settings.isManual) // Remote control is only valid in remote mode
	{
		// ['char width'] Command
		// [1] $
		// [1] R
		// [1] ,
		// [5] Joystick X = -Left / +Right
		// [1] ,
		// [5] Joystick Y = +AccelForward / -AccelBackward
		// [1] ,
		// [1]Brake
		// [1] ,
		// [1]Cruise
		// $1,34567,90123,5,7* <-- positions

		// Get Left / Right
		strncpy(tStr, lStr + sizeof(char) * 3, 5);
		tStr[5] = '\0';
		uint8_t newSteering = atoi(tStr);

		// Get Forward / Backward
		strncpy(tStr, lStr + sizeof(char) * 9, 5);
		tStr[5] = '\0';
		int16_t newTorque = atoi(tStr);

		// Get Brake
		bool newBrake = (lStr[15] == '0') ? false : true;

		// Get Cruise
		bool newCruise = (lStr[17] == '0') ? false : true;

		// Load new values in to Drive object
		drive.setDesiredSteerAngle(newSteering);
		drive.setDesiredDriveTorque(newTorque);
		drive.setControlVars(newBrake, newCruise);
		settings.CommandAge = 0; // reset command counter
	}
	else if (lStr[1] == 'C')
	{
		// ['char width'] Command
		// [1] $
		// [1] C
		// [1] ,
		// [3] setting subject
		// [1] ,
		// [8] (+ / -) (new value) RIGHT ALIGNED, 0 Padded -- +0000011 = +11
		// $1,345,78901234* <-- positions
		
		if (strncmp(lStr + sizeof(char) * 3, "man", 3) == 0)
		{
			settings.isManual = !settings.isManual;
			sprintf(retStr, "Manual mode is now set to %d\r\n", settings.isManual);
		}
		else if (strncmp(lStr + sizeof(char) * 3, "geo", 3) == 0)
		{
			settings.isGeoFenced = !settings.isGeoFenced;
			sprintf(retStr, "Geofence mode is now set to %d\r\n", settings.isGeoFenced);
		}
		else if (strncmp(lStr + sizeof(char) * 3, "den", 3) == 0)
		{
			settings.driveEnabled = (lStr[14] == '0') ? false : true;
			sprintf(retStr, "Motor enable is now set to %d\r\n", settings.driveEnabled);
		}
		else if (strncmp(lStr + sizeof(char) * 3, "gps", 3) == 0)
		{
			if ((lStr[7] == 'r'))
			{
				settings.reportGPS = !settings.reportGPS;
				sprintf(retStr, "GPS Reporting is now set to %d\r\n", settings.reportGPS);
			}
			else if ((lStr[7] == 's'))
			{
				settings.GPSshowSats = !settings.GPSshowSats;
				if (settings.GPSshowSats == true)
				{
					strcpy(settings.GPSString, "$PMTK314,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
					settings.newGPS = true;
					sprintf(retStr, "Now showing GPS Satellites\r\n", settings.reportGPS);
				}
				else
				{
					strcpy(settings.GPSString, "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
					settings.newGPS = true;
					sprintf(retStr, "Now showing GPS Location\r\n", settings.reportGPS);
				}
			}
		}
		else if (strncmp(lStr + sizeof(char) * 3, "imu", 3) == 0)
		{
			settings.reportIMU = !settings.reportIMU;
			sprintf(retStr, "IMU Reporting is now set to %d\r\n", settings.reportIMU);
		}
		else if (strncmp(lStr + sizeof(char) * 3, "ums", 3) == 0)
		{
			// Get update ms value
			strncpy(tStr, lStr + sizeof(char) * 7, 8);
			tStr[8] = '\0';
			settings.setUpdateMillis(atoi(tStr));
			sprintf(retStr, "Update loop is now set to %dms\r\n", settings.getUpdateMillis());
		}
		else
		{
			sprintf(retStr, errorUnrecognised);
		}
	}
	else
	{
		sprintf(retStr, errorUnrecognised);
	}

	// Return Confirmation
	BT_Send(retStr, strlen(retStr));
}

void BT::initUSARTPins()
{
	// Set up PINs that will be used first
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);	// Define specific AF on pin 2 (USART2 - TX)
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);	// Define specific AF on pin 3 (USART2 - RX)
	GPIO_InitTypeDef GPIO_InitStruct;
	// Initialize pins as alternate function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;			// Pick which pin(s) to init - Pin 6/7
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;					// Set pin(s) mode to AF
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;					// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;					// Set internal resistor to pull up
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;				// 50MHz pin clock, easily fast enough for 9600bps
	GPIO_Init(GPIOA, &GPIO_InitStruct);							// Set data on selected port using struct
}

void BT::initUSART(uint32_t uiBaudRate, uint16_t uiMode)
{
	// USART Setup Information
	// Set up USART Peripheral Device - 1382400 8 N 1
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);			// Give the USART a clock
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate = uiBaudRate;					// Set Baud Rate to 9600bps
	USART_InitStruct.USART_WordLength = USART_WordLength_8b; 		// Set Word Length to 8
	USART_InitStruct.USART_Parity = USART_Parity_No;				// Set Parity to N
	USART_InitStruct.USART_StopBits = USART_StopBits_1;				// Set Stop Bits to 1
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // Disable hardware flow control
	USART_InitStruct.USART_Mode = uiMode;							// Enable both Tx and Rx
	USART_Init(USART2, &USART_InitStruct);							// Send data to USART

	// Set up interrupt controller for usart
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;					// Set IRQ channel to USART2
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;					// Enable Interrupts for USART2
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;			// Set USART to highest priority
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;					// Set USART to highest sub priority
	NVIC_Init(&NVIC_InitStruct);

	// Clear any current interrupt flags
	USART_ClearITPendingBit(USART2, USART_IT_RXNE);	// Clear USART 2 RX interrupt bit
	USART_ClearITPendingBit(USART2, USART_IT_TXE);	// Clear USART 2 TX interrupt bit

	// Enable USART Module / RX and TX interrupts
	USART_Cmd(USART2, ENABLE);						// Enable USART Module
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);	// Enable USART6 RX Interrupt
	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);	// Disable USART6 TX Interrupt (Enable when there is something to send)
}

bool BT::RXBuffer::add(char newChar)
{
	if (newChar == '$')
	{
		currentChar = 0;
	}
	if (currentChar == 0 && newChar != '$')
	{
		return false;
	}

	// Check for overflow, if string is too long start again
	if (currentChar > 195)
	{
		currentChar = 0;
		currentCRC = 0;
		crcLocal = 0;
		return false;
	}

	if (currentCRC > 0)
	{
		if (currentCRC == 1)
		{
			crcRx = (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0')) << 4;
		}
		else if (currentCRC == 2)
		{
			crcRx += (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0'));
		}
		++currentCRC;
	}
	else
	{
		if (newChar == '*') // Check for end of string
		{
			// Start Recieving CRC
			currentCRC = 1;
		}
		else if (currentChar > 0) // Not end of string, add new char
		{
			// Update checksum with every char after the first $ until *
			crcLocal ^= newChar;
		}
	}

	// Add new char in to array
	data[currentChar++] = newChar;

	return true;

}

bool BT::RXBuffer::checkComplete()
{
	if (currentChar > 2)
	{
		if (data[currentChar - 3] == '*')
		{
			return true;
		}
	}
	return false;
}

void BT::RXBuffer::move(char * GPSString)
{
	data[currentChar] = '\0';
	strncpy(GPSString, data, currentChar);
	currentChar = 0;
	currentCRC = 0;
	crcLocal = 0;
	crcRx = 0;
}

// Useful Strings - Static
const char BT::errorCRC[] = "Invalid Command: CRC Error.\r\n";
const char BT::errorUnrecognised[] = "Invalid Command: Unrecognised.\r\n";
const char BT::noticeProjectInitComplete[] = "RoboKart initialisation complete.\r\n";