#include "GPS.h"

GPS::GPS() : GPSString(""), isGPSStringNew(false)
{
	GPSData.initialLat = 0.0f;
	GPSData.initialLong = 0.0f;
	GPSData.currentLat = 0.0f;
	GPSData.currentLong = 0.0f;
	GPSData.isFirst = true;
	GPSData.isComplete = false;
}

void GPS::initialise()
{
	// Set up RXBuffer
	RXBuffer.currentChar = 0;

	// Initialise USART Pins
	initUSARTPins();

	// Initialise USART
	initUSART(57600, USART_Mode_Rx | USART_Mode_Tx);

	// Configure Circular Buffer
	TXBuffer.Initialise(USART1_BASE);
}

void GPS::GPS_Recieve(char byte)
{
	// Add byte to buffer
	if (!RXBuffer.add(byte))
	{
		// Oh... the buffer is full... this is a problem
		return;
	}

	// Check that string is complete
	if (RXBuffer.checkComplete())
	{
		// Copy Complete String
		RXBuffer.move(GPSString);

		// Set flag
		isGPSStringNew = true;
	}
}

void GPS::GPS_Send(char * string, uint16_t length)
{
	// Verify String (check for $ at start, \r\n at end) (Datasheet p12)
	if (string[0] != '$' || string[length - 2] != '\r' || string[length - 1] != '\n')
	{
		return;
	}

	// Add string to TX buffer
	TXBuffer.PushBack(string, length);
}

void GPS::registerBT(BT * newbt)
{
	bt = newbt;
}

void GPS::ParseString()
{
//	isGPSStringNew = true;
//	strcpy(GPSString, "$GPGGA,053740.000,2503.6319,N,12136.0099,E,1,08,1.1,63.8,M,15.2,M,,0000*64");
	static uint8_t count = 0;

	// send new command to GPS if required
	if (settings.newGPS)
	{
		GPS_Send(settings.GPSString, strlen(settings.GPSString));
		settings.newGPS = false;
	}

	// Check that the string is new
	if (!isGPSStringNew)
	{
		return;
	}

	// report GPS if required
	if (settings.reportGPS)
	{
		if (count++ == 10)
		{
			bt->BT_Send(GPSString, strlen(GPSString));
			count = 0;
		}
	}

	// set current GPS data to invalid
	GPSData.isComplete = false;

	// GPS String is no longer new
	isGPSStringNew = false;

	// Create required vars for parsing
	char lStr[192];	// Buffer to hold currently worked on string
	char * strPtr;	// Pointer for strtok

	// Copy current string in to working string
	strcpy(lStr, GPSString);

	// Break string apart by tokens (GPS starts with $, separated by comma, ends with *)
	strPtr = strtok(lStr, "$,*");

	// Check for GPS string type...
	if ((strPtr != NULL) && (strcmp("GPGGA", strPtr) == 0))			// GGA
	{
		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// UTC Time
		GPSData.UTCTime = atof(strPtr);

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// Latitude
		GPSData.currentLat = atof(strPtr);

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// North/South
		if (strPtr[0] == 'S')
		{
			GPSData.currentLat = -GPSData.currentLat;
		}

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// Longitude
		GPSData.currentLong = atof(strPtr);

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// East/West
		if (strPtr[0] == 'W')
		{
			GPSData.currentLong = -GPSData.currentLong;
		}

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// Position Fix
		GPSData.positionFix = atoi(strPtr);

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// Satellites Used (not needed)

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// Horizontal Dilution of Precision (not needed)

		// Move to next token and check for validity
		strPtr = strtok(NULL, "$,*");
		if (strPtr == NULL)
		{
			return;
		}
		// MSL Altitude 
		GPSData.mslAltitude = atof(strPtr);

		// End here, no more fields required
		settings.GPSAge = 0; // reset gps counter
		GPSData.isComplete = true;
	}
	else if ((strPtr != NULL) && (strcmp("GPRMC", strPtr) == 0))	// RMC
	{
		
	}

	if (GPSData.isFirst)
	{
		GPSData.initialLat = GPSData.currentLat;
		GPSData.initialLong = GPSData.currentLong;
		GPSData.isFirst = false;
	}

	if (settings.GPSAge == 0)
	{
		calcDistanceFromStart();
	}
}

void GPS::calcDistanceFromStart()
{
	// Code sourced from http://www.codecodex.com/wiki/Calculate_distance_between_two_points_on_a_globe#C.2B.2B
	double dlat = (GPSData.initialLat - GPSData.currentLat) * DEGREE_TO_RAD; // find change in lon
	double dlon = (GPSData.initialLong - GPSData.currentLong) * DEGREE_TO_RAD; // find change in lat

	double latH = sin(dlat * 0.5);
	latH *= latH;
	double lonH = sin(dlon * 0.5);
	lonH *= lonH;

	double tmp = cos(GPSData.initialLat * DEGREE_TO_RAD) * cos(GPSData.currentLat * DEGREE_TO_RAD);

	// Using australian national spheroid
	// 6 378 160m (http://www.ga.gov.au/scientific-topics/positioning-navigation/geodesy/geodetic-datums/historical-datums-of-australia/australian-geodetic-datum-agd)
	settings.GPSDistanceFromInitial = EARTH_RAD_M * (2.0 * asin(sqrt(latH + tmp*lonH)));
}

void GPS::initUSARTPins()
{
	// USART Setup Information
	// Set up PINs that will be used first
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);	// Define specific AF on pin 6 (USART1 - TX)
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);	// Define specific AF on pin 7 (USART1 - RX)
	GPIO_InitTypeDef GPIO_InitStruct;
	// Initialize pins as alternate function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;			// Pick which pin(s) to init - Pin 6/7
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;					// Set pin(s) mode to AF
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;					// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;					// Set internal resistor to pull up
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;				// 50MHz pin clock, easily fast enough for 9600bps
	GPIO_Init(GPIOB, &GPIO_InitStruct);							// Set data on selected port using struct
}

void GPS::initUSART(uint32_t uiBaudRate, uint16_t uiMode)
{
	// Set up USART Peripheral Device - 9600 8 N 1
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);			// Give the USART a clock
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate = uiBaudRate;					// Set Baud Rate to 9600bps
	USART_InitStruct.USART_WordLength = USART_WordLength_8b; 		// Set Word Length to 8
	USART_InitStruct.USART_Parity = USART_Parity_No;				// Set Parity to N
	USART_InitStruct.USART_StopBits = USART_StopBits_1;				// Set Stop Bits to 1
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // Disable hardware flow control
	USART_InitStruct.USART_Mode = uiMode;							// Enable both Tx and Rx
	USART_Init(USART1, &USART_InitStruct);							// Send data to USART

	// Set up interrupt controller for usart
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;					// Set IRQ channel to USART1
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;					// Enable Interrupts for USART1
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;			// Set USART to highest priority
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;					// Set USART to highest sub priority
	NVIC_Init(&NVIC_InitStruct);

	// Clear any current interrupt flags
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);	// Clear USART 1 RX interrupt bit
	USART_ClearITPendingBit(USART1, USART_IT_TXE);	// Clear USART 1 TX interrupt bit

	// Enable USART Module / RX and TX interrupts
	USART_Cmd(USART1, ENABLE);						// Enable USART Module
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	// Enable USART1 RX Interrupt
	USART_ITConfig(USART1, USART_IT_TXE, DISABLE);	// Disable USART1 TX Interrupt (Enable when there is something to send)
}

bool GPS::RXBuffer::add(char newChar)
{
	if (newChar == '$')
	{
		currentChar = 0;
	}
	if (currentChar == 0 && newChar != '$')
	{
		return false;
	}
	// Check for overflow, if string is too long start again
	if (currentChar > 195)
	{
		currentChar = 0;
		currentCRC = 0;
		crcLocal = 0;
		return false;
	}

	if (currentCRC > 0)
	{
		if (currentCRC == 1)
		{
			crcRx = (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0')) << 4;
		}
		else if (currentCRC == 2)
		{
			crcRx += (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0'));
		}
		++currentCRC;
	}
	else
	{
		if (newChar == '*') // Check for end of string
		{
			// Start Recieving CRC
			currentCRC = 1;
		}
		else if (currentChar > 0) // Not end of string, add new char
		{
			// Update checksum with every char after the first $ until *
			crcLocal ^= newChar;
		}
	}

	// Add new char in to array
	data[currentChar++] = newChar;

	return true;
	
}

bool GPS::RXBuffer::checkComplete()
{
	if (currentChar > 2)
	{
		if (data[currentChar - 2] == '\r' && data[currentChar - 1] == '\n')
		{
			return true;
		}
	}
	return false;
}

void GPS::RXBuffer::move(char * GPSString)
{
	data[currentChar] = '\0';
	strncpy(GPSString, data, currentChar);
	currentChar = 0;
}