#include "Drive.h"

Drive::Drive() : driveEnable(false),
				 steerEnable(false),
				 dAngle(0),
				 dTorque(0),
				 desiredSteerAngle(0),
				 desiredDriveTorque(0)
{

}
void Drive::Initialise(uint16_t dSpeed, uint8_t dAngle)
{
	// Set brake, cruise and reverse to high (off)
	GPIO_SetBits(GPIOE, GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);

	// Set angle and torque change per step
	this->dAngle = dAngle;
	this->dTorque = dSpeed;

	// Initialise Enable Pins
	InitEnablePins();
	InitControlOutputPins();

	// Initialise Hardware Modules
	InitPWM();
	InitDAC();
	InitADC();

	// Ensure all power is disabled
	setPower(false, false);

	// Set initial steering and torque values
	setDesiredSteerAngle(175); // Set steering to centre
	setDesiredDriveTorque(0); // Set torque to 0
}

void Drive::InitEnablePins()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);	// Enable clock on port E
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);	// Enable clock on port B

	// Init GPIO for Brake / Cruise / Reverse / Spare (PE8 - PE15)
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;			// Set pin(s) mode to Output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set pin(s) output type to push/pull
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		// Set pin(s) speed to reasonable level
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s) (Externally fitted)

	// Drive Motor Enable
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;				// Pick which pin(s) to init - Pin 7
	GPIO_Init(GPIOE, &GPIO_InitStruct);					// Set data on selected port (E) using struct
	GPIO_ResetBits(GPIOE, GPIO_Pin_7);

	// Steering Servo Enable
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11;				// Pick which pin(s) to init - Pin 11
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// Set data on selected port (B) using struct
	GPIO_ResetBits(GPIOB, GPIO_Pin_11);					// Ensure pin is disabled initially
}

void Drive::InitControlOutputPins()
{
	// Init GPIO for Brake / Cruise / Reverse / Spare (PE8 - PE15)
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = 0xFF00;					// Pick which pin(s) to init - Pin 8 to 15
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;			// Set pin(s) mode to Output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set pin(s) output type to push/pull
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		// Set pin(s) speed to reasonable level
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s) (Externally fitted)
	GPIO_Init(GPIOE, &GPIO_InitStruct);					// Set data on selected port using struct
}

void Drive::InitPWM()
{
	// Set up pins that will be used
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_TIM2);	// Define specific AF on pin 10 (TIM2)
	GPIO_InitTypeDef GPIO_InitStruct;
	// Initialize pins as alternate function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;						// Pick which pin(s) to init - Pin 10
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;					// Set pin(s) mode to AF
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;					// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;					// Set internal resistor to pull up
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;				// 50MHz pin clock, easily fast enough for 9600bps
	GPIO_Init(GPIOB, &GPIO_InitStruct);							// Set data on selected port using struct

	// Set up for PWM
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);		// Enable Timer 2 block (Give it a clock)
	TIM_TimeBaseInitTypeDef timerInitStruct;
	timerInitStruct.TIM_Prescaler = 840;						// Set timer prescaler to ???? (sysclock divider)
	//  - Timer Tick Frequency => 84MHz / (1679 + 1) = 50kHz
	timerInitStruct.TIM_Period = 249;							// Set timer period (reload value)
	//  - Period => 84MHz / (4199 + 1) = 500Hz (2ms)
	timerInitStruct.TIM_CounterMode = TIM_CounterMode_Up;		// Set timer counter mode to count up
	timerInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;			// Set clock division
	timerInitStruct.TIM_RepetitionCounter = 0;					// Set number of timer repetitions to 0 before update event is generated and number resets (only for TIM1 and TIM8)
	TIM_TimeBaseInit(TIM2, &timerInitStruct);					// Load init data into Timer 2
	TIM_Cmd(TIM2, DISABLE);										// DISABLE Timer2

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 101;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_Low;

	TIM_OC3Init(TIM2, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
}

void Drive::InitDAC()
{
	// Enable AF on DAC Pins (PA4 and PA5) - Consumes 280.56uA @ 25 degrees @ 168MHz (page 96)
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);	// Enable clock on port A
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;	// Pick which pin(s) to init - Pin 4/5
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;			// Set pin(s) mode to Analogue Mode
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set pin(s) output type to push/pull
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOA, &GPIO_InitStruct);					// Set data on selected port using struct

	// Init DAC
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	DAC_InitTypeDef DAC_InitStructure;
	DAC_StructInit(&DAC_InitStructure);
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;	// Enable DAC output Buffer
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;	// Disable Waveform Generation
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;				// Disable DAC trigger (use manual for now)
	DAC_Init(DAC_Channel_1, &DAC_InitStructure);					// Configure DAC CH1
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);					// Configure DAC CH2

	// Ensure motors are initialised to stopped and forward
	GPIO_SetBits(GPIOA, GPIO_Pin_4 | GPIO_Pin_5);				// Set both motors to forward (reverse is active low)
	DAC_SetDualChannelData(DAC_Align_12b_R, 0x0000, 0x0000);	// Set both DACs at the same time, better consistency between motors

	// DISABLE DAC Channels
	DAC_Cmd(DAC_Channel_1, DISABLE);
	DAC_Cmd(DAC_Channel_2, DISABLE);
}

void Drive::InitADC()
{
	// Enable AF on ADC Pin (PB0)
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);	// Enable clock on port B
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;				// Pick which pin(s) to init - Pin 0
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;			// Set pin(s) mode to Analogue Mode
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set pin(s) output type to push/pull
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// Set data on selected port using struct

	// Init Hardware Clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	// Init Common ADC values
	ADC_CommonInitTypeDef ADC_CommonInitStruct;
	ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStruct);

	// Init ADC1
	ADC_InitTypeDef ADC_InitStruct;
	ADC_StructInit(&ADC_InitStruct);
	ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;	// Set ADC resolution to 12b, plenty of time for conversion (2ms)
	ADC_InitStruct.ADC_ScanConvMode = DISABLE;			// Single channel conversion mode
	ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;	// Set to single shot mode - Triggered by software 2ms before update
	ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None; // No external trigger
	ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;	// Set ADC data to right alignment
	ADC_InitStruct.ADC_NbrOfConversion = 1;				// 1 conversion / signal
	ADC_Init(ADC1, &ADC_InitStruct);

	// Configure ADC Channel
	ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1, ADC_SampleTime_28Cycles);

	// Configure ADC Interrupts
	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = ADC_IRQn;				// Set the IRQ channel to ADC
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 1;	// Set the interrupt priority, lower is more important
	nvicStructure.NVIC_IRQChannelSubPriority = 1;			// Set the interrupt sub-priority, lower is more important
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;				// Set the interrupt to enabled
	NVIC_Init(&nvicStructure);								// Send the struct to init the interrupt

	// Enable ADC Interrupt
	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);

	// Enable the ADC
	ADC_Cmd(ADC1, ENABLE);
}

void Drive::setPower(bool driveEnable, bool steerEnable)
{
	setDrivePower(driveEnable);
	setSteerPower(steerEnable);
}

bool Drive::zeroControls()
{
	// Set steering angle to current value
	desiredSteerAngle = currentSteerAngle;
	// Assume kart will only re-enable from stationary (IMU stuff?)
	currentDriveTorque = 0;
	desiredDriveTorque = 0;
	// Zero drive offset, will be recalculated when moving
	driveOffset = 0;
}

void Drive::setDrivePower(bool driveEnable)
{
	// Check that there is a change to be processed
	if (this->driveEnable == driveEnable)
	{
		return;
	}

	// Disable timer to ensure nothing in this sequence is interrupted
	TIM_Cmd(TIM4, DISABLE);
	
	// Zero all variables for safety (Freeze steering)
	zeroControls();
	// Load zero'd variables in to hardware
	loadTorqueAndAngle();

	// Perform necessary operations for changes
	if (driveEnable == true)
	{
		// Ensure DAC is enabled
		DAC_Cmd(DAC_Channel_1, ENABLE);
		DAC_Cmd(DAC_Channel_2, ENABLE);
		// Turn on enable pin
		GPIO_SetBits(GPIOE, GPIO_Pin_7);
	}
	else
	{
		// Turn off enable pin
		GPIO_ResetBits(GPIOE, GPIO_Pin_7);
		// Ensure DAC is disabled
		DAC_Cmd(DAC_Channel_1, DISABLE);
		DAC_Cmd(DAC_Channel_2, DISABLE);
	}

	// Copy new info across to variable
	this->driveEnable = driveEnable;

	// Reenable timer
	TIM_Cmd(TIM4, ENABLE);
}

void Drive::setSteerPower(bool steerEnable)
{
	// Check that there is a change to be processed
	if (this->steerEnable == steerEnable)
	{
		return;
	}

	// Disable timer to ensure nothing in this sequence is interrupted
	TIM_Cmd(TIM4, DISABLE);

	// Zero all variables for safety (Freeze steering)
	zeroControls();
	// Load zero'd variables in to hardware
	loadTorqueAndAngle();

	if (steerEnable == true)
	{
		// Ensure PWM is enabled
		TIM_Cmd(TIM2, ENABLE);
		// Turn on enable pin
		GPIO_SetBits(GPIOB, GPIO_Pin_11);
	}
	else
	{
		// Turn off enable pin
		GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		// Ensure PWM is disabled
		TIM_Cmd(TIM2, DISABLE);
	}

	// Copy new info across to variable
	this->steerEnable = steerEnable;

	// Reenable timer
	TIM_Cmd(TIM4, ENABLE);
}

bool Drive::setDesiredSteerAngle(uint8_t steerAngle)
{
	// Clip steering angle to limits
	if (steerAngle < 101)
	{
		steerAngle = 101;
	}
	else if (steerAngle > 169)
	{
		steerAngle = 169;
	}

	// Set steering angle
	if (steerAngle > 100 && steerAngle < 170)
	{
		desiredSteerAngle = steerAngle;
		return true;
	}
	else
	{
		return false;
	}
}

bool Drive::setDesiredDriveTorque(int16_t driveTorque)
{
	if (driveTorque > -4096 && driveTorque < 4096)
	{
		desiredDriveTorque = driveTorque;
		return true;
	}
	else
	{
		return false;
	}
}

void Drive::update()
{
	// Change from Auto to Manual Mode if required
	if (settings.isManual && settings.driveEnabled)
	{
		setPower(true, false);	// Steering servo off to allow manual steering
		if (driveReverse)
		{
			setDesiredDriveTorque(-accelValue);	// Set accel value as desired torque
		}
		else
		{
			setDesiredDriveTorque(accelValue);	// Set accel value as desired torque
		}
	}
	else if (!settings.isManual && settings.driveEnabled)
	{
		// Make sure there is no reason to shut down
		if (settings.isGeoFenced && (settings.GPSDistanceFromInitial > settings.GPSDistanceFromInitialMax || settings.GPSAge >= settings.GPSAgeMax))
		{
			settings.driveEnabled = false; // must be manually reenabled to prevent flapping
			zeroControls(); // set torque to 0 to avoid sudden restart
		}
		else if (settings.CommandAge >= settings.CommandAgeMax)
		{
			zeroControls(); // set torque to 0, no command recieved
			setPower(true, true);	// Steering servo on to allow auto steering
		}
		else
		{
			setPower(true, true);	// Steering servo on to allow auto steering
		}
	}
	else
	{
		setPower(false, false);
		zeroControls();
	}

	// Update the Torque / angle
	setCurrentAngleAndTorque();
	// Update wheel differentials
	driveOffset = calculateDriveOffset(currentSteerAngle);
	// Load new Torque / angle in to registers
	loadTorqueAndAngle();
}

bool Drive::setCurrentAngleAndTorque()
{
	// Check if the steer angle has changed
	if (currentSteerAngle != desiredSteerAngle)
	{
		currentSteerAngle = desiredSteerAngle;
		/*** Speed now controlled by hardware ***
		// Check if the number needs to go up or down
		if (currentSteerAngle < desiredSteerAngle)
		{
			// number needs to go up to desiredSteerAngle
			currentSteerAngle = std::min(desiredSteerAngle, (uint8_t)(currentSteerAngle + dAngle));
		}
		else
		{
			// number needs to go down to desiredSteerAngle
			currentSteerAngle = std::max(desiredSteerAngle, (uint8_t)(currentSteerAngle - dAngle));
		}
		*/
	}

	// Check if the drive Torque has changed
	if (currentDriveTorque != desiredDriveTorque)
	{

		if (settings.isManual == true)
		{
			currentDriveTorque = desiredDriveTorque;
		}
		else
		{
			// Check if the number needs to go up or down
			if (currentDriveTorque < desiredDriveTorque)
			{
				// number needs to go up to desiredDriveTorque
				currentDriveTorque = std::min(desiredDriveTorque, (int16_t)(currentDriveTorque + dTorque));
			}
			else
			{
				// number needs to go down to desiredDriveTorque
				//currentDriveTorque = std::max(desiredDriveTorque, (int16_t)(currentDriveTorque - dTorque));
				currentDriveTorque = desiredDriveTorque; // Torque must be able to be dropped immediately
			}
		}
	}
	return true;
}

int16_t Drive::calculateDriveOffset(uint8_t angle)
{

	return 0;
}

bool Drive::loadTorqueAndAngle()
{
	// Declare vars
	uint16_t driveLeft, driveRight;

	// Load Steering Angle
	TIM2->CCR3 = currentSteerAngle;
	// Figure out wheel differentials
	if (currentDriveTorque >= 0)
	{
		// Driving Forward
		GPIO_SetBits(GPIOE, GPIO_Pin_10 | GPIO_Pin_14);
		driveLeft = currentDriveTorque - driveOffset;
		driveRight = currentDriveTorque + driveOffset;
	}
	else
	{
		// Driving Backward
		GPIO_ResetBits(GPIOE, GPIO_Pin_10 | GPIO_Pin_14);
		driveLeft = -currentDriveTorque - driveOffset;
		driveRight = -currentDriveTorque + driveOffset;
	}

	// Load Drive Torque
	DAC_SetDualChannelData(DAC_Align_12b_R, driveLeft, driveRight);

	// Debug Output
#ifdef DEBUG
		//printf("Drive::loadTorqueAndAngle -> GPIOE: 0x%04X, DriveLeft: 0x%04X, DriveRight: 0x%04X\r\n", GPIO_ReadOutputData(GPIOE), driveLeft, driveRight);
#endif

	// Return
	return true;
}

void Drive::setAccelValue(uint16_t newAccelValue)
{
	// Scale Value to calibration data
	// ADC - Output => 0V = 0, 4096 = 3V3
	// Amplified Output => 0V = 0, 4.94V = 4096 (1.206mV per step)
	// Final value for motors needs to be... nothing, values is pretty good 
	if (newAccelValue < 1000)
	{
		newAccelValue = 0;
	}

	// Copy new data across, masked to ensure validity
	accelValue = (0x0FFF & newAccelValue);
}

void Drive::setReverse(bool newReverse)
{
	driveReverse = newReverse;
}

void Drive::setControlVars(bool brake, bool cruise)
{
	// Set / Reset Brake
	if (brake)
	{
		GPIO_ResetBits(GPIOE, GPIO_Pin_8 | GPIO_Pin_12);
	}
	else
	{
		GPIO_SetBits(GPIOE, GPIO_Pin_8 | GPIO_Pin_12);
	}

	// Set / Reset Cruise
	if (cruise)
	{
		GPIO_ResetBits(GPIOE, GPIO_Pin_9 | GPIO_Pin_13);
	}
	else
	{
		GPIO_SetBits(GPIOE, GPIO_Pin_9 | GPIO_Pin_13);
	}
	
}