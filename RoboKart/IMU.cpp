#include "IMU.h"

IMU::IMU()
{

}

void IMU::initialise()
{
#ifdef DEBUG
	printf("IMU::Initialise -> IMU Init Started\n");
#endif
	// Initialise Chip Select Pins
	initCSPins();
	// Initialse SPI Hardware
	initSPI();
	// Initialise IMU Settings
	initIMU();
#ifdef DEBUG
	printf("IMU::Initialise -> IMU Init Complete\n");
#endif
}

void IMU::getAccelData(float * fltData)
{
	// Set up variables
	uint8_t spiData[6];
	int16_t data[3];

	// Read data from chip
	spiReadBytes(0, OUT_X_L_A, spiData, 6);

	// Format data in to 16b numbers
	data[0] = (spiData[1] << 8) | spiData[0];
	data[1] = (spiData[3] << 8) | spiData[2];
	data[2] = (spiData[5] << 8) | spiData[4];

#ifdef DEBUG
	printf("IMU::getAccelData -> Accel Data (RAW)     X: %5d    Y: %5d   Z: %5d\n", data[0], data[1], data[2]);
#endif

	// Scale data based on vars to get g
	for (int i = 0; i < 3; i++)
	{
		fltData[i] = data[i] * IMU_A_SCALE;
	}
#ifdef DEBUG
	printf("IMU::getAccelData -> Accel Data (SCALED)  X: %5f    Y: %5f   Z: %5f\n", fltData[0], fltData[1], fltData[2]);
#endif
}

void IMU::getGyroData(float * fltData)
{
	// Set up variables
	uint8_t spiData[6];
	int16_t data[3];

	// Read data from chip
	spiReadBytes(1, OUT_X_L_G, spiData, 6);

	// Format data in to 16b numbers
	data[0] = (spiData[1] << 8) | spiData[0];
	data[1] = (spiData[3] << 8) | spiData[2];
	data[2] = (spiData[5] << 8) | spiData[4];

#ifdef DEBUG
	printf("IMU::getGyroData -> Gyro Data  (RAW)     X: %5d    Y: %5d   Z: %5d\n", data[0], data[1], data[2]);
#endif

	// Scale data based on vars to get deg/s (float)
	for (int i = 0; i < 3; i++)
	{
		fltData[i] = data[i] * IMU_G_SCALE;
	}

#ifdef DEBUG
	printf("IMU::getGyroData -> Gyro Data  (SCALED)  X: %5f    Y: %5f   Z: %5f\n", fltData[0], fltData[1], fltData[2]);
#endif

}

void IMU::getMagData(float * fltData)
{
	// Set up variables
	uint8_t spiData[6];
	int16_t data[3];

	// Read data from chip
	spiReadBytes(0, OUT_X_L_M, spiData, 6);

	// Format data in to 16b numbers
	data[0] = (spiData[1] << 8) | spiData[0];
	data[1] = (spiData[3] << 8) | spiData[2];
	data[2] = (spiData[5] << 8) | spiData[4];

#ifdef DEBUG
	printf("IMU::getMagData -> Mag Data   (RAW)     X: %5d    Y: %5d   Z: %5d\n", data[0], data[1], data[2]);
#endif

	// Scale data based on vars to get Gs
	for (int i = 0; i < 3; i++)
	{
		fltData[i] = data[i] * IMU_M_SCALE;
	}

#ifdef DEBUG
	printf("IMU::getMagData -> Mag Data   (SCALED)  X: %5f    Y: %5f   Z: %5f\n", fltData[0], fltData[1], fltData[2]);
#endif
}

void IMU::getTempData(float * fltData)
{
	// Set up variables
	uint8_t spiData[2];
	int16_t data;

	// Read data from chip
	spiReadBytes(0, OUT_TEMP_L_XM, spiData, 2);

	// Format data in to 16b numbers
	data = (spiData[1] << 8) | spiData[0];

#ifdef DEBUG
	printf("IMU::getTempData -> Temp Data  (RAW)     T: %5d\n", data);
#endif

	// Scale data based on vars to get Temp
	data = data << 4;
	*fltData = data;
	*fltData = ( *fltData / 16.0) * IMU_T_SCALE;

#ifdef DEBUG
	printf("IMU::getTempData -> Temp Data  (SCALED)  T: %5f\n", *fltData);
#endif
}

void IMU::initCSPins()
{
	// Set up Chip Select Pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE); // enable clock to hardware
	GPIO_InitTypeDef GPIO_InitStruct;					// prepare struct for init
	GPIO_InitStruct.GPIO_Pin = IMU_CS_PINS;				// Pick which pin(s) to init - Pin 12/11/10
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;			// Set pin(s) mode to output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// Set internal resistor to pull up
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_25MHz;		// 25MHz pin clock, easily fast enough for 400k
	GPIO_Init(GPIOD, &GPIO_InitStruct);					// Write data to hardware

	// Set initial CS pin state (both idle)
	GPIO_SetBits(GPIOD, IMU_CS_PINS);
}

void IMU::initSPI()
{
	// Set up SPI
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);			// Enable clock to hardware
	SPI_InitTypeDef SPI_InitStruct;									// Prepare struct for init
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;	// Max SPI Speed
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;	// Full Duplex Comms (MISO / MOSI)
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;						// MCU is master
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;					// 8 bits per packet
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;							// Software will perform slave select
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;					// Most significant bit first (from timing diagram)
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_High;						// Clock Polarity (from timing diagram)
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge;						// Clock Edge for capture - 2nd edge (from timing diagram)
	SPI_Init(SPI3, &SPI_InitStruct);

	// Set up alternate function pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_10;	// Pick which pin(s) to init - Pin 12/11/10
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;						// Set pin(s) mode to AF
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;						// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;					// Set internal resistor to off
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;					// 25MHz pin clock, easily fast enough for 400k
	GPIO_Init(GPIOC, &GPIO_InitStruct);

	// Define Specifc AF to be used on AF pins
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_SPI3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);

	// Enable SPI
	SPI_Cmd(SPI3, ENABLE);
}

bool IMU::initIMU()
{
	// Set up required vars
	uint8_t check[4];
	
	// Confirm Device is working (WhoAmI)
	check[0] = spiReadByte(0, 0x0F);
	check[1] = spiReadByte(1, 0x0F);

#ifdef DEBUG
	printf("IMU::initIM -> XM WhoAmI: 0x%02x\n", check[0]);
	printf("IMU::initIM -> G  WhoAmI: 0x%02x\n", check[1]);
#endif
	if (check[0] != 0x49 || check[1] != 0xD4) return false; // Expected Return: (0x49) and (0xD4)

	// Initialise Accel and check for errors (Datasheet P55)
	spiWriteByte(0, CTRL_REG0_XM, 0x00); // Normal mode, no filters
	spiWriteByte(0, CTRL_REG1_XM, 0x5F); // 50Hz, continuous update, all axes
	spiWriteByte(0, CTRL_REG2_XM, 0x00); // No filters, 6g, 4 wire SPI
	check[0] = spiReadByte(0, CTRL_REG0_XM);
	check[1] = spiReadByte(0, CTRL_REG1_XM);
	check[2] = spiReadByte(0, CTRL_REG2_XM);
#ifdef DEBUG
	printf("IMU::initIM -> XM Config (Write): 0x00, 0x57, 0x00\n");
	printf("IMU::initIM -> XM Config  (Read): 0x%02x, 0x%02x, 0x%02x\n", check[0], check[1], check[2]);
#endif
	if ((check[0] != 0x00) || (check[1] != 0x57) || (check[2] != 0x00)) return false;

	// Initialise Mag / Temperature and check for errors (Datasheet P55)
	spiWriteByte(0, CTRL_REG5_XM, 0x88); // TEMP_EN, Low Res, 12.5Hz
	spiWriteByte(0, CTRL_REG6_XM, 0x00); // MFS = +/- 2 gauss
	spiWriteByte(0, CTRL_REG7_XM, 0x00);
	check[0] = spiReadByte(0, CTRL_REG5_XM);
	check[1] = spiReadByte(0, CTRL_REG6_XM);
	check[2] = spiReadByte(0, CTRL_REG7_XM);
#ifdef DEBUG
	printf("IMU::initIM -> XM Config (Write): 0x88, 0x00, 0x00\n");
	printf("IMU::initIM -> XM Config  (Read): 0x%02x, 0x%02x, 0x%02x\n", check[0], check[1], check[2]);
#endif
	if ((check[0] != 0x82) || (check[1] != 0x00)) return false;

	// Initialise Gyro and check for errors (Datasheet P41)
	spiWriteByte(0, CTRL_REG1_G, 0x0F); // 95Hz ODR, Normal Mode, All Axis enabled
	spiWriteByte(0, CTRL_REG2_G, 0x00); // No HPF
	spiWriteByte(0, CTRL_REG3_G, 0x88); // INT_G Enable, Push/pull
	spiWriteByte(0, CTRL_REG4_G, 0x00); // continuous, LSB, 245dps, 4 wire SPI - 0 0 00 0 00 0
	check[0] = spiReadByte(0, CTRL_REG1_G);
	check[1] = spiReadByte(0, CTRL_REG2_G);
	check[2] = spiReadByte(0, CTRL_REG3_G);
	check[3] = spiReadByte(0, CTRL_REG4_G);
#ifdef DEBUG
	printf("IMU::initIM -> XM Config (Write): 0x0F, 0x00, 0x88, 0x00\n");
	printf("IMU::initIM -> XM Config  (Read): 0x%02x, 0x%02x, 0x%02x, 0x%02x\n", check[0], check[1], check[2], check[3]);
#endif
	if ((check[0] != 0x0F) || (check[1] != 0x00) || (check[3] != 0x88) || (check[4] != 0x00)) return false;

	return true;
}

void IMU::spiWriteByte(uint8_t chip, uint8_t address, uint8_t data)
{
	// Set up first byte (Read + Increment + address)
	uint8_t addrData = 0x00 | (address & 0x3F); 

	selectChip(chip);	// Select chip
	spiTransfer(addrData);	// Send Address (8 Bits)
	spiTransfer(data);	// Send Data (8 Bits)

	// Set all chip select pins back to high
	GPIO_SetBits(GPIOD, IMU_CS_PINS);
}

uint8_t IMU::spiReadByte(uint8_t chip, uint8_t address)
{
	// Set up first byte (Read + Increment + address)
	uint8_t addrData = 0x80 | (address & 0x3F);

	selectChip(chip);	// Select chip
	spiTransfer(addrData);	// Send Address (8 Bits)
	uint8_t data = spiTransfer(0x00); // Return recieved data

	// Set all chip select pins back to high
	GPIO_SetBits(GPIOD, IMU_CS_PINS);

	return data;
}

void IMU::spiReadByte(uint8_t chip, uint8_t address, uint8_t * data)
{
	// Set up first byte (Read + Increment + address)
	uint8_t addrData = 0x80 | (address & 0x3F);

	selectChip(chip);	// Select chip
	spiTransfer(addrData);	// Send Address (8 Bits)
	*data = spiTransfer(0x00);	// place recieved data in to variable

	// Set all chip select pins back to high
	GPIO_SetBits(GPIOD, IMU_CS_PINS);
}

void IMU::spiReadBytes(uint8_t chip, uint8_t address, uint8_t * data, uint8_t length)
{
	// Set up first byte (Read + Increment + address)
	uint8_t addrData = 0xC0 | (address & 0x3F);

	selectChip(chip);	// Select chip
	spiTransfer(addrData);	// Send Address (8 Bits)
	for (int i = 0; i < length; i++)
	{
		data[i] = spiTransfer(0x00);	// place recieved data in to variable
	}

	// Set all chip select pins back to high
	GPIO_SetBits(GPIOD, IMU_CS_PINS);
}

void IMU::selectChip(uint8_t chip)
{
	switch (chip)
	{
	default:
	case 0: // Accelerometer Active
		GPIO_ResetBits(GPIOD, IMU_CS_XM); // Set Accel / Magnet low
		GPIO_SetBits(GPIOD, IMU_CS_G); // Set gyro high
		break;
	case 1: // Gyroscope Active
		GPIO_SetBits(GPIOD, IMU_CS_XM); // Set Accel / Magnet high
		GPIO_ResetBits(GPIOD, IMU_CS_G); // Set gyro low
		break;
	}
}

uint8_t IMU::spiTransfer(uint8_t data)
{
	while (!SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE)); // wait for SPI flag
	SPI_I2S_SendData(SPI3, data); // send data (or keep clock going)
	while (!SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE)); // wait for SPI flag
	return SPI_I2S_ReceiveData(SPI3); //Clear RXNE bit and return data
}