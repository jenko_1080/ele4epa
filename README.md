# README #

This is open to public so I should probably have some detail here...

First up, core details:
This code is for my 4th year Electronic Engineering project, the overall outcome is to design some form of automated go kart. The microcontroller used here is the STM32F407VG.

At some point in the near future a blog will be online:
 - Probably at this URL: http://www.thecrushinator.com/blog/

Comments are welcome as I'm keen to improve.

There are a few choices I've made not entirely for performance, but for practice. The most obvious one here is using C++ and classes instead of C / structs.
Saying that, I am aware that C++11 has some performance improvements over the older C++ so when I find out about them I'll implement them over time.


### To run this code... ###

I'm using VisualGDB to compile and run this code with an STM32F4-Discovery board.
Everything is pretty easy because of this and as far as I can tell (if you VisualGDB and Visual Studio 2013) it's a matter of download and compile.

### Contact ###

Currently I remain elusive... but as stated in the long winded first bit, there will be a blog somewhere around here:
http://www.thecrushinator.com/blog/